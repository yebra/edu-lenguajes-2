<html>

<head>
	<title>Multiplos en PHP</title>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF8">
</head>

<body>

	<div id="enunciado" style="background-color: lightgray;">
		<h1>Enunciado</h1>
		<p>Esta primera tarea que os planteo se trata de generar, utilizando PHP, una página
		que contenga una división centrada, de 800px de anchura, separada del margen superior 
		por 50px y con background gris. En su interior deben ir listados, separados por comas,
		 los 5000 primeros números múltiplos de 3 o de 5.</p>

		<p>Ejemplo: <code>3,5,6,9,10...</code></p>

	</div>

	<h1>Resultado</h1>

<!--TODO: Tu solución aquí
<?php


?>-->

</body>

</html>
