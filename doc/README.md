# Instrucciones de documentación

La primera consideración es que **NO debes editar ningún fichero de README.md para tu propia documentación**. Si quieres corregir algún error ortográfico o de cualquier tipo, realízalo a través de un pull request.

Si quieres anexar imágenes a tu documentación asegúrate de almacenarlas **todas** en `doc/img` y en formato `png` a poder ser.

Para documentar tu código debes contemplar los siguientes aspectos:


## Documenta el procedimiento que has seguido

#### Ejercicio en Java

Utiliza la carpeta `doc/java/Work.md` donde documentes (con screenshots si lo ves conveniente) todo el proceso de instalación del entorno de desarrollo/ejecución, resolución del ejercicio, compilación, empaquetado y prueba.

* Resuelve el ejercicio en `java/src/Multiplos.java`
* Genera el bytecode en `java/classes/Multiplos.class`
* Empaqueta tu solución en `java/dist/Multiplos.jar`

#### Ejercicio en C++

Utiliza la carpeta `doc/c++/Work.md` donde documentes (con screenshots si lo ves conveniente) todo el proceso de instalación del entorno de desarrollo/ejecución, resolución del ejercicio, compilación y prueba.

* Resuelve el ejercicio en `c++/src/Multiplos.c`
* Ubica el código objeto en `c++/dist/Multiplos`

#### Ejercicio en Python

Utiliza la carpeta `doc/python/Work.md` donde documentes (con screenshots si lo ves conveniente) todo el proceso de instalación del entorno de desarrollo/ejecución, resolución del ejercicio y prueba.

* Resuelve el ejercicio en `python/Multiplos.py`

#### Ejercicio en Php

Utiliza la carpeta `doc/php/Work.md` donde documentes (con screenshots si lo ves conveniente) todo el proceso de instalación del entorno de desarrollo/ejecución, resolución del ejercicio y prueba.



* Resuelve el ejercicio en `php/index.php`

## Mecanismo de versionado

En la resolución del ejercicio utiliza este sistema de versionado:

**Importante:** Sé estricto y preciso formando la cadena de la versión. Utiliza caracteres [ASCII](https://es.wikipedia.org/wiki/ASCII) (sin acentos). Pon tu nombre con la primera letra en minúscula siguiendo el resto con el estilo [lowerCamelCase](https://es.wikipedia.org/wiki/CamelCase).

```Shell
# Ejemplo
git tag java-v1.0-juanCarlosDeBorbon
```

* `lenguaje-v1.x-nombreApellido1`: donde `lenguaje`representa el lenguaje utilizado y donde `x` representa un número secuencial en cada incremento.